package primzahlen;
import java.util.Arrays;

public class EratosthenesSieb {

	public static void main(String[] args) {
		int schrittweite, zaehler;
		
		// array initialisieren und mit ture fuellen
		boolean[] zahlen = new boolean[10000 - 2];
		Arrays.fill(zahlen, true);
		
		for (int y = 0; y < zahlen.length / 2 + 1; y++) {
			// um 2 versetzt + y fuer aktuelle zahl | UNNOETIGE RECHENSCHRITTE schrittweite sollte immer weiter zur n�chsten prim springen
			schrittweite = 2 + y;
			// schrittweite + y fuer startwert (um basiszahl in der reihe zu ueberspringen)
			zaehler = schrittweite + y;
			for (int i = 0; i < zahlen.length; i++) {
				// out of bound
				if (zaehler >= zahlen.length) break;
				
				// zahl ist keine primzahl
				zahlen[zaehler] = false;
				// zaehler um schrittweite erh�hen
				zaehler += schrittweite;
			}
		}
		
		// Primzahlen ausgeben
		for (int i = 0; i < zahlen.length; i++) {
			if (zahlen[i]) {
				System.out.print((i + 2) + ", ");
			}
		}
	}
}
