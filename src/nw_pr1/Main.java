package nw_pr1;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;
import java.util.TreeSet;

public class Main {

	public static void main(String[] args) {
		// Aufgabe 4
		try {
			Zahlenzaehler();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Aufgabe 5
		//Set<int> treeSet = new TreeSet<>(IntegerComparator);
	}

	private static void Zahlenzaehler() throws IOException {
		File numberCount = null;
		File testfile = new File("test.txt");
		int count = 0;

		BufferedReader reader = new BufferedReader(
				new InputStreamReader(new FileInputStream(testfile), Charset.forName("UTF-8")));
		int c;
		while ((c = reader.read()) != -1) {
			char character = (char) c;
			if (c >= 48 && c <= 57) {
				count++;
			}
		}

		// try block von https://www.w3schools.com/java/java_files_create.asp
		try {
			Path path = Paths.get("out/numberCount.txt");
			Files.createDirectories(path.getParent());
			numberCount = new File(path.toString());
			if (numberCount.createNewFile()) {
				System.out.println("File created: " + numberCount.getName());
			} else {
				System.out.println("File already exists.");
			}
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}

		try {
			FileWriter myWriter = new FileWriter(numberCount);
			myWriter.write(count + "");
			myWriter.close();
			System.out.println("Successfully wrote to the file.");
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}

	}
}
