package exeptions;

public class Main {

	public static void main(String[] args) {
		boolean bool = false;
		
		// A
		try {
			
			// B
			try {
				if (bool) throw new Exe2();
				if (!bool) throw new Exe1();
			} catch (Exe2 e) {
				System.out.println("catchB");
			} finally {
				System.out.println("finB");
			}
			
		} catch(Exe1 e) {
			System.out.println("catchA");
		} finally {
			System.out.println("finA");
		}

	}

}
