package nw_pr1_a3;

public class Ferien {
	public static void main(String[] args) {
	Unterkunft[] unterkuenfte = new Unterkunft[6];
	unterkuenfte[0] = new Sheraton("Tokio", 4);
	unterkuenfte[1] = new Sheraton("Teneriffa", 5);
	unterkuenfte[2] = new Hilton("New York", 5);
	unterkuenfte[3] = new Hilton("London", 4);
	unterkuenfte[4] = new Appartement("San Diego", "Sunstar", 17);
	unterkuenfte[5] = new Appartement("Sarasota", "Sandy-Beach", 3);
	uebernachten(unterkuenfte);
	}
	public static void uebernachten(Unterkunft[] unterkuenfte) {
		for (int i = 0; i < unterkuenfte.length; i++) {
			unterkuenfte[i].uebernachten();
		}
	}
}
