package nw_pr1_a3;

public abstract class Unterkunft {
	private String ort;
	
	public Unterkunft(String s) {
		ort = s;
	}
	
	public String getOrt() {
		return ort;
	}
	
	abstract public void uebernachten();
}
