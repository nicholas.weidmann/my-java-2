package nw_pr1_a3;

public class Appartement extends Unterkunft{
	private String name;
	private int number;
	
	public Appartement(String s, String ss, int i) {
		super(s);
		name = ss;
		number = i;
	}
	
	@Override
	public void uebernachten() {
		System.out.println("Das Appartement heisst "+name+", hat die Nummer "+number+" und ist in "+getOrt());
	}
}
