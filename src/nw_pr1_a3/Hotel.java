package nw_pr1_a3;

public abstract class Hotel extends Unterkunft {
	private int sterne;
	
	public Hotel(String s, int i) {
		super(s);
		sterne = i;
	}
	
	public int getSterne() {
		return sterne;
	}
	
	abstract public void essen();
}
