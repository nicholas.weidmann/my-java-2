package sqrt;

public class Sqrt {

	public static void main(String[] args) {
		double y, A, yneu;
		int counter = 0;
        A = 12;
        y = 1;
        while (y * y != A) {
            System.out.println(counter + ") Y: " + y + ", Fl�che: " + A);
            yneu = (y + A / y) / 2;
            if (yneu == y) {
                // We have a problem here, we can't get more precise so this should be the result
                break;
            }
        	counter++;
            y = yneu;
        }
        System.out.println("Berechnet mit aritmetischer Mitte: " + y);
        System.out.println("Berechnet mit math.sqrt:           " + Math.sqrt(A));
	}
}
